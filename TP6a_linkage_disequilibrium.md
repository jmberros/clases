# Haploview

Los usuarios avanzados pueden desear un control más fino sobre la región en la que se muestra LD. Para esto se puede utilizar el programa Haploview (Barrett et al. 2005), que permite generar un análisis del desequilibrio de ligamiento de manera local, a partir de varios formatos de archivo.

## Conseguir datos de variantes para graficar el LD

Antes que nada, asegúrese de que se encuentra en el directorio del TP:

```bash
cd ~/TP6a
```

    💡 TIP: El tilde (~) significa “el directorio home del usuario”; por ej. /home/student

Lo que necesitamos primero son datos de input con variantes en muestras de distintas poblaciones, para poder visualizar el LD con Haploview. El browser de Ensembl da la opción de exportar datos en formato “Haploview”, pero sólo lo permite en regiones de menos de 20 kb, de modo que recurriremos a otra estrategia: descargar directamente de los servidores de 1000 Genomas utilizando el programa tabix.

`tabix` permite descargar las variantes encontradas en todas las muestras de 1000 Genomas para una o varias regiones específicas de cierto cromosoma. Las variantes se descargan en formato vcf. En la carpeta `~/TP6` ya encontrará el archivo `LCT_region.vcf`, de modo que no necesitará usar tabix. Con fines informativos, el comando corrido para descargar el vcf fue el siguiente:

```bash
tabix -fh ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz 2:136544424-136595736 > LCT_region.vcf
```

El archivo `LCT_region.vcf` contiene todos los genotipos de las variantes descriptas por 1000 Genomas en la región `2:136544424-136595736` (donde se encuentra el gen LCT, coordenadas del assembly GRCh37). Para graficar el LD, nos interesa quedarnos con los SNPs bialélicos, dado que Haploview sólo acepta este tipo de marcadores. Además, nos interesa graficar el LD de una población por vez, de modo de poder comparar los gráficos entre poblaciones diferentes.

Los siguientes comandos del programa PLINK nos servirán para filtrar los SNPs bialélicos de MAF > 0,01 pertenecientes a las poblaciones YRI y CEU:

```bash
plink --vcf LCT_region.vcf --recode HV --biallelic-only strict --snps-only --keep-fam YRI.samples --out LCT_biallelic_YRI

plink --vcf LCT_region.vcf --recode HV --biallelic-only strict --snps-only --keep-fam CEU.samples --out LCT_biallelic_CEU
```

**Nota**: Los archivos YRI.samples y CEU.samples tienen listas de IDs de individuos de esas poblaciones. Para este TP ya están en la carpeta, listos para usar, pero en otra clase aprenderemos cómo generarlos.
Lea rápidamente el log de PLINK impreso en pantalla para cada comando. ¿Con cuántos SNPs e individuos nos quedamos en cada uno de los pasos anteriores?

PLINK guarda el output de sus comandos en archivos `.log`. Fíjese Usted mismo que estos archivos `.log` fueron generados:

```bash
ls -lh
```

Para no gastarse los ojos buscando las líneas que le interesan, utilice el comando grep para buscar rápidamente en los logs:

```bash
grep 'pass filters' *.log
```

Si bien el archivo que generamos con PLINK está en formato “HV” (por Haploview), al intentar cargarlo con este programa aparecerá un error. Corregiremos el formato del archivo siguiendo la documentación de Haploview. Con este fin, la cátedra escribió un script de Python que ejecutaremos directamente:

```bash
./scripts/ped_to_haploview.py LCT_biallelic_YRI.chr-2.ped
./scripts/ped_to_haploview.py LCT_biallelic_CEU.chr-2.ped
```

    💡 TIP: Puede explorar el script con un editor de texto como gedit.

```bash
gedit ./scripts/ped_to_haploview.py
```

Lea el script rápidamente --no importa si no entiende la mayor parte. Encuentre la función `ped_line_to_haploview_line`, que procesa una línea de formato PED uniendo los primeros campos con tabs, pero los genotipos con un espacio, el arreglo necesario para que Haploview acepte el archivo de input.

Explore cómo quedaron los archivos que utilizaremos de input para Haploview:

```bash
less -S LCT_biallelic_CEU.chr-2.haploview.ped
```
    
Más allá de variaciones menores, las primeras seis columnas del formato PED tienen los IDs de familia e individuo, padre, madre, código de sexo y fenotipo. El resto de las columnas tienen los genotipos de esa muestra, para cada marcador.

La lista de marcadores se da en un archivo diferente, `.info`, con la condición de que estén listados en el mismo orden que las columnas del `.ped`. Explore el archivo `.info` correspondiente al `.ped` que acaba de leer:

```bash
less LCT_biallelic_CEU.chr-2.info
```

Se reportan sólo posiciones e IDs de marcadores.

## Ejecutar el programa Haploview

Los archivos `LCT_biallelic_{YRI,CEU}.chr-2.haploview.ped` junto a sus `.info` deberían servir para cargar la información en Haploview. Ejecutemos el programa, que ya fue descargado en la carpeta `/scripts` del TP:

```bash
java -jar ./scripts/Haploview.jar
```

Se abrirá la interfaz gráfica de Haploview. Elija el “Linkage Format”. Como “Data File” debe elegir uno de los archivos `.haploview.ped` que generamos previamente y como “Locus Information File”, el archivo `.info` correspondiente a la misma población. Elija los correspondientes a la población CEU:

```bash
# Seleccione estos archivos en Haploview:

~/TP6a/LCT_biallelic_CEU.chr-2.haploview.ped
~/TP6a/LCT_biallelic_CEU.chr-2.info
```

Si cliquea “OK” se cargarán los datos. Explore el programa. Hay cuatro vistas: LD Plot, Haplotypes, Check Markers, Tagger.

La sección “Check Markers” es la primera que se abre. Puede seleccionar los SNPs que son utilizados para el LD Plot y para los Haplotypes. Los marcadores pueden ser agregados o removidos del análisis manualmente, mediante el uso de los checkboxes. La información en la tabla puede ser ordenada cliqueando en los títulos de las columnas.

    `#` Es un índice para la tabla.
    Name es el identificador del marcador.
    Position es la posición del marcador.
    ObsHET es la heterocigosidad observada.
    PredHET es la heterocigosidad predicha (2 * MAF * (1 - MAF)).
    HWpval es el p-value en un test de equilibrio Hardy-Weinberg. Es decir, se trata de la probabilidad de que la desviación del HWE pueda ser explicada por el azar.
    %Geno es el porcentaje de genotipos no faltantes para este marcador.
    FamTrio es el número de tríos genotipados completamente para el marcador (0 para marcadores sin individuos relacionados).
    MendErr es el número de errores mendelianos observados (0 para marcadores sin individuos relacionados).
    MAF es la frecuencia de alelo menor (usando solo fundadores) para este marcador.
    Alleles alelo mayor y menor para este marcador.
    Rating está chequeado si el marcador pasa todos los tests y sin chequear si falla uno o más test (resaltados en rojo).

Puede cambiar los valores de los filtros y luego seleccionar “Rescore Markers” para filtrar de acuerdo a los nuevos parámetros.

## Exportar el gráfico de LD

La pestaña “LD Plot” contiene el gráfico de desequilibrio de ligamiento. Estadísticas detalladas de LD para cada comparación entre marcadores puede ser visualizadas haciendo click derecho sobre cualquier punto del gráfico.

Click derecho en el número de un marcador muestra su nombre y frecuencia de alelo menor.

Se puede hacer zoom in y zoom out usando la opción de zoom en el menú “Display” (especialmente útil para regiones muy amplias). Para datasets largos también se muestra un mini-mapa de la región en la esquina inferior izquierda.

El esquema de colores y el espaciamiento de los marcadores para el gráfico de LD puede ser ajustado en el menú “Display”.

Exporte el gráfico de LD como una imagen utilizando el menú File > Export current tab to PNG, y guarde la imagen como CEU_LD_plot.png.

## Bloques de LD

Cambie a la pestaña 'Haplotypes'. Los haplotipos son estimados usando un algoritmo EM acelerado similar al método “partition/ligation” descripto en Qin et al., 2002, Am J Hum Genet, pero la estrategia de agrupamiento puede cambiarse en el menú `Analysis > Define Blocks`. Las opciones son las siguientes, descriptas en la documentación:

** Confidence Intervals [DEFAULT] **

*The default algorithm is taken from Gabriel et al, Science, 2002. 95% confidence bounds on D prime are generated and each comparison is called "strong LD", "inconclusive" or "strong recombination". A block is created if 95% of informative (i.e. non-inconclusive) comparisons are "strong LD". This method by default ignores markers with MAF < 0.05. The MAF cutoff and the confidence bound cutoffs can be edited by choosing "Customize Block Definitions" (Analysis menu). This definition allows for many overlapping blocks to be valid. The default behavior is to sort the list of all possible blocks and start with the largest and keep adding blocks as long as they don't overlap with an already declared block.*

** Four Gamete Rule **

*This is a variant on the algorithm described in Wang et al, Am. J. Hum. Genet., 2002. For each marker pair, the population frequencies of the 4 possible two-marker haplotypes are computed. If all 4 are observed with at least frequency 0.01, a recombination is deemed to have taken place. Blocks are formed by consecutive markers where only 3 gametes are observed. The 1% cutoff can be edited to make the definition more or less stringent.*

** Solid Spine of LD **

*This internally developed method searches for a "spine" of strong LD running from one marker to another along the legs of the triangle in the LD chart (this would mean that the first and last markers in a block are in strong LD with all intermediate markers but that the intermediate markers are not necessarily in LD with each other).*

¿Cuántos bloques de LD se generan con el algoritmo utilizado por default? ¿Cuántos haplotipos diferentes se observaron en cada bloque?

## Generar una lista de tag SNPs

En la pestaña “Tagger” se puede seleccionar un conjunto de tag SNPs para la región. El Tagger de Haploview comienza seleccionando un conjunto mínimo de marcadores tales que todos los alelos de la región sean capturados con un r2 superior al umbral que el usuario especifique (0,8 por default; puede cambiarse en “r2 threshold”). Manualmente se pueden excluir o incluir SNPs en el conjunto de tag-SNPs que se generará algorítmicamente (cliqueando en “Force Include” o “Force Exclude”).

Si además se elige la estrategia de aggressive tagging, se añaden dos pasos adicionales. En primer lugar, se intentan capturar los SNPs no capturados por otro SNP en el paso anterior mediante una batería de multi-marker tests. A continuación, se intenta reducir la lista de tag SNPs reemplazando algunos SNPs con el resultado de los multi-marker tests.

Para ejecutar el algoritmo de tagging en modo pairwise, cliquee en el boton “Run tagger”. Se abrirá la pestaña de resultados.

¿Cuántos SNPs fueron seleccionados como tag SNPs, y a alelos en total permiten capturar?

La lista de tag SNPs puede guardarse en un archivo cliqueando en “Dump Tags File”. Guarde el archivo como CEU_tag_SNPs en la carpeta del TP.

Luego, desde la terminal, explore el archivo generado. ¿Tiene algún dato extra además de los rs IDs de cada SNP? ¿Coincide el número de SNPs con el número reportado por la interfaz gráfica?

```bash
head CEU_tag_SNPs
wc -l CEU_tag_SNPs
```

Visualice ahora el LD con Haploview para la población YRI. Los pasos serán iguales que para la población CEU, pero seleccionando al comienzo los archivos .haploview.ped y .info de YRI. Guarde el gráfico de LD como PNG y compárelo con el generado para CEU. ¿Nota diferencias de desequilibrio de ligamiento en la región entre ambas poblaciones?

Mire la sección Haplotypes. ¿La cantidad de bloques de LD es la misma en YRI que para la población CEU?

Corra el Tagger para la población YRI. ¿Cuántos tag SNPs son necesarios para la región? ¿Más o menos que para la población CEU? ¿Coincide esto con sus expectativas?
