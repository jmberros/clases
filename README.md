# TPs de Genómica de Poblaciones y Enfermedades Humanas, 2016

Friendly link to this page: http://cor.to/genomicatps

* [TP6a: Linkage Disequilibrium](./TP6a_linkage_disequilibrium.md)
* [TP6b: VCFTools](./TP6b_VCFtools.md)
* TP7: Population Genetics Workshop (by John Novembre)
* ~~TP8a: Selection Browser~~
* TP8b: Signals of Selection: Fst, PBS and iHS in FIN vs. CHB
* [TP9: Genome Wide Association Studies with PLINK](./TP9_gwas_plink.md)
* [TP10: Variant Calling Pipeline](./TP10_variant_calling_pipeline.md)
